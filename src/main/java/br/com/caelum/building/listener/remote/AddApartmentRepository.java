package br.com.caelum.building.listener.remote;

import br.com.caelum.building.domain.Building;

import java.util.Optional;

public interface AddApartmentRepository {
    void save(Building building);
    Optional<Building> findById(Long id);
}
