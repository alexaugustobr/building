package br.com.caelum.building.api.registration;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.springframework.http.ResponseEntity.created;

@RestController
@AllArgsConstructor
class RegistrationController {
    private final RegistrationService service;

    @PostMapping
    ResponseEntity<?> create(@RequestBody BuildingInput input, UriComponentsBuilder uriBuilder) {
        Long id = service.registerBy(input);

        URI uri = uriBuilder.path("{id}").build(id);

        return created(uri).build();
    }

    @Data
    static class BuildingInput {
        private Integer numberOfBlocks;
        private String condominiumName;
        private String address;
    }
}
