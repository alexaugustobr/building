package br.com.caelum.building.api.describe;

import br.com.caelum.building.api.describe.DescriberController.BuildingOutput;
import br.com.caelum.building.domain.Building;
import br.com.caelum.building.shared.Mapper;
import org.springframework.stereotype.Component;

@Component
class BuildingToBuildingOutput implements Mapper<Building, BuildingOutput> {
    @Override
    public BuildingOutput map(Building source) {
        return new BuildingOutput(source.getId(), source.getCondominiumName(), source.getAddress(), source.getNumberOfBlocks(), source.getNumberOfUnits());
    }
}
