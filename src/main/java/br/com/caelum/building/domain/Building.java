package br.com.caelum.building.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Entity
@RequiredArgsConstructor
@NoArgsConstructor
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull
    private String condominiumName;

    @NonNull
    @NotNull
    private String address;

    @NotNull
    @NonNull
    private Integer numberOfBlocks;

    @NotNull
    @NonNull
    private Integer numberOfUnits;

    public void incrementApartment() {
        numberOfUnits = numberOfUnits + 1;
    }
}
