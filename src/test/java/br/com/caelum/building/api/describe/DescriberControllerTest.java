package br.com.caelum.building.api.describe;

import br.com.caelum.building.api.describe.DescriberController.BuildingOutput;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.BDDMockito.given;

@SpringBootTest
@AutoConfigureRestDocs(outputDir = "target/snippets")
class DescriberControllerTest {

    @MockBean
    private DescribeService service;


    @BeforeEach
    void setup() {
        BuildingOutput output = new BuildingOutput(1L, "Santa Julia",
                "Rua vergueiro, 3185", 1, 13);


        given(service.showBy(1L)).willReturn(output);

        RestAssuredMockMvc.standaloneSetup(new DescriberController(service))
        ;
    }
}